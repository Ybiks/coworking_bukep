import React from 'react';
import ReactDOM from 'react-dom'
import {Link} from 'react-router'


class PersonalAccountInformation extends React.Component{
	render(){
		return (
				<div>
					<div className="row">
						<div className="col col--sm-3 text--xs-center">
							<Link className="a_account_link" activeClassName="a_account_link-active" style={{backgroundColor: '#2ecc71', color: 'white'}}>
								<h1>2048</h1>
								<h3>Бонусных баллов</h3>
							</Link>
						</div>
						<div className="col col--sm-3 text--xs-center">
							<Link className="a_account_link" activeClassName="a_account_link-active" style={{backgroundColor: '#e74c3c', color: 'white'}}>
								<h1>11</h1>
								<h3>Подписок на категории</h3>
							</Link>
						</div>
						<div className="col col--sm-3 text--xs-center">
							<Link to="/personal-account/basket" className="a_account_link" activeClassName="a_account_link-active" style={{backgroundColor: '#1abc9c', color: 'white'}}>
								<h1>16</h1>
								<h3>Товаров в корзине</h3>
							</Link>
						</div>
						<div className="col col--sm-3 text--xs-center">
							<Link className="a_account_link" activeClassName="a_account_link-active" style={{backgroundColor: '#9b59b6', color: 'white'}}>
								<h1>2</h1>
								<h3>Оставленно отзывов</h3>
							</Link>
						</div>
					</div>
				</div>
		)
	}
}


export default PersonalAccountInformation;