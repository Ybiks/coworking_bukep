import locale from "../libs/locale";

export default function(
	state = locale.transfer(locale.determineLanguage()),
	action
) {
	switch (action.type) {
		case "SET_LANGUAGE":
			return (function() {
				var transfer = locale.transfer(
					locale.setLanguageInCookie(action.payload)
				);
				return transfer instanceof Function ? transfer : state;
			})();

		default:
			return state;
	}
}
