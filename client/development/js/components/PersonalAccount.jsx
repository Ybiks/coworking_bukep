import React from 'react';
import ReactDOM from 'react-dom'
import {Link} from 'react-router'
import {HistoryElement} from '../libs/HistoryElement.jsx'


class PersonalAccount extends React.Component{
	render(){
		return (
			<HistoryElement pathname="/personalAccount">
				<div className="div_container_padding">
					<div className="grid grid--container grid--no-gutters">
						<div className="row">
							<div className="col col--sm-6 col--xs-6 text--xs-left text--sm-left">
								<Link to="/logout" className="button_loggout">
									<i className="icon">&#xe810;</i>
									Редактировать
								</Link>
							</div>
							<div className="col col--sm-6 col--xs-6 text--xs-right text--sm-right">
								<Link to="/logout" className="button_loggout">
									<i className="icon">&#xe80b;</i>
									Выйти
								</Link>
							</div>
						</div>

						<div className="row">
							<div className="col col--sm-12 col--xs-12 text--xs-center text--sm-center" style={{fontSize: 0}}>
								<img className="avatar" src="/public/images/thumb-83907.jpg"/>
							</div>
						</div>

						<div className="row">
							<div className="col col--sm-12 col--xs-12 text--xs-center text--sm-center">
								<h2>{this.props.state.user.name + ' '+ this.props.state.user.surname}</h2>
							</div>
						</div>
						{this.props.children}
					</div>
				</div>
			</HistoryElement>
		)
	}
}


export default PersonalAccount;


/*
<hr/>
						<div className="row row--no-gutters">
							<div className="col col--sm-3 col--xs-6 col--no-gutters text--xs-center text--sm-center">
								<Link to="/personal-account/basket" className="color_blue a_account_link" activeClassName="color_blue a_account_link-active">
									<h2>15</h2>
									
									<p>Корзина</p>
								</Link>
							</div>
							<div className="col col--sm-3 col--xs-6 col--no-gutters text--xs-center text--sm-center">
								<Link to="" className="color_yellow a_account_link" activeClassName="">
									<h2>0</h2>
									
									<p>В пути</p>
								</Link>
							</div>
							<div className="col col--sm-3 col--xs-6 col--no-gutters text--xs-center text--sm-center">
								<Link to="" className="color_green a_account_link" activeClassName="">
									<h2>2</h2>
									<p>Подтверждено</p>
								</Link>
							</div>
							<div className="col col--sm-3 col--xs-6 col--no-gutters text--xs-center text--sm-center">
								<Link to="" className="color_biloba_flower a_account_link" activeClassName="">
									<h2>1</h2>
									
									<p>Отзыв</p>
								</Link>
							</div>
						</div>
						<hr/>*/