import React, { Component } from "react";
import ReactDOM from "react-dom";
import { Link } from "react-router";
import { HistoryElement } from "../../libs/HistoryElement.jsx";

class Lease extends Component {
	constructor(props){
		super(props);

		//Если window имеется, значит это не сервер, и мы можем "лениво" загрузить скрипт google map
		if(window !== void 0){
			console.log('CLIENT SCRIPT');

			let lazyScript = window.document.createElement('script');
			lazyScript.async = true;
			lazyScript.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDG6zD5QGwF1c8B3vRrtHghVm0WI-poEjA&libraries=places&callback=initAutocomplete'
			
			window.document.body.appendChild(lazyScript);
		};



		this.initAutocomplete = this.initAutocomplete.bind(this);

	}

	//Фунуция collback
	initAutocomplete(){
		let google = window.google,
			map = new google.maps.Map(this.map, {
				center: {lat: 55.75222, lng: 37.61140},
				zoom: 17,
				mapTypeId: 'roadmap'
			});

		// Create the search box.
		let searchBox = new google.maps.places.SearchBox(this.searchBox);

		// Bias the SearchBox results towards current map's viewport.
		map.addListener('bounds_changed', function() {
			searchBox.setBounds(map.getBounds());
		});

		 let markers = [];
		// Listen for the event fired when the user selects a prediction and retrieve
		// more details for that place.
		searchBox.addListener('places_changed', function() {
			let places = searchBox.getPlaces();

			if (places.length == 0) {
				return;
			}

			// Clear out the old markers.
			markers.forEach(function(marker) {
				marker.setMap(null);
			});
			markers = [];

			// For each place, get the icon, name and location.
			let bounds = new google.maps.LatLngBounds();
			places.forEach(function(place) {
				if (!place.geometry) {
					console.log("Returned place contains no geometry");
					return;
				}
				let icon = {
					url: place.icon,
					size: new google.maps.Size(71, 71),
					origin: new google.maps.Point(0, 0),
					anchor: new google.maps.Point(17, 34),
					scaledSize: new google.maps.Size(25, 25)
				};

				// Create a marker for each place.
				markers.push(new google.maps.Marker({
					map: map,
					title: place.name,
					position: place.geometry.location
				}));

				if (place.geometry.viewport) {
					// Only geocodes have viewport.
					bounds.union(place.geometry.viewport);
				} else {
					bounds.extend(place.geometry.location);
				}
			});
			map.fitBounds(bounds);
		});
	}

	componentDidMount(){
		if(window === void 0) return;//Если window отсутствует, значит это серверный рендеринг. На сервере крту мы не рендерим

		window.initAutocomplete = this.initAutocomplete;

		if(window.google !== void 0){
			this.initAutocomplete();
		}
	
	}
	render() {
		return (
			<HistoryElement
				pathname="/lease"
				style={{
					backgroundColor: "#ebeff1",
					overflowY: "auto",
					height: "100%"
				}}
				className="lease"
			>
				<div className="row lease_container">
					<LeaseHeader>
						<input className="input_text-primary" ref={el => this.searchBox = el} placeholder="Адрес дома"/>
					</LeaseHeader>
					<div className="col col--lg-8 col--md-7 col--sm-5 lease_helper">
						<div ref={el => this.map = el} className="lease_map"></div>
					</div>
				</div>
			</HistoryElement>
		);
	}
}

export default Lease;

const LeaseHeader = (props) =>
	<div className="col col--lg-4 col--md-5 col--sm-7 lease_description">
		<div className="lease_header">
			<h1 className="lease_header-h1">
				Сдать квартиру в аренду
			</h1>
		</div>
		{props.children}
	</div>

/*class Adress extends Component(
	render(){
		return (
			<input className="input_text-primary" ref={el => this.searchBox = el} placeholder="Адрес дома"/>
		)
	}
);
*/