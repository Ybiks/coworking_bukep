import Home from './Home.jsx';
import Flat from './Flat.jsx';
import Lease from './Lease.jsx';
import Tenant from './Tenant.jsx';
import NotFound from './NotFound.jsx';

export {
	Home,
	Flat,
	Lease,
	Tenant,
	NotFound
}