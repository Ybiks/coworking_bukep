import { polyfill } from "es6-promise";
polyfill();

import fetch from 'isomorphic-fetch';

export default function() {
	return fetch('http://192.168.101.1:801/api/flats', {method: 'get'}).then(res => res.json())
}
