import React from "react";
import ReactDOM from "react-dom";

const historyScroll = {};

class HistoryElement extends React.Component {
	componentDidMount() {
		this.refs[this.props.pathname].scrollTop =
			historyScroll[this.props.pathname] || 0;
	}

	componentWillUnmount() {
		historyScroll[this.props.pathname] = this.refs[
			this.props.pathname
		].scrollTop;
	}

	render() {
		return (
			<div
				{...{
					style: this.props.style,
					className: this.props.className
				}}
				ref={this.props.pathname}
			>
				{this.props.children}
			</div>
		);
	}
}

const deletePath = path => {
	delete historyScroll[path];
};

export { HistoryElement, deletePath };
