import path from "path";
import React from "react";
import Cache from "./cache";
import express from "express";
import ReactDOM from "react-dom/server";
import clientMarkup from "../client/development/js/markup.jsx";

global.cache = new Cache();

const page = function(config) {
	let { title, keywords, markup } = config;
	return `
		<!html>
			<head>
				<title>${title}</title>
				<meta charset='utf-8'>
				<meta name="keywords" itemprop="keywords" content="${keywords}">
				<meta name="viewport" content="width=device-width, initial-scale=1">
				<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
				<link rel="stylesheet" type="text/css" href="/public/css/style.css">
			</head>
			<body>
				<div id="app">${markup}</div>
				<script src="/public/js/bundle.js"></script>
			</body>
		</html>
	`;
};

export default function(app) {
	if (!app) var app = express();

	app
		.disable("x-powered-by") //Удаление заголовка express из http

		.get("/", function(req, res, next) {
			res.setHeader("Access-Control-Allow-Origin", "*"); //Hазрешаем кросс-доменные запросы
			next();
		})

		.use("/api/getDefaultState", function(req, res) {
			res.send(JSON.stringify({
				flats: {
					flatsList: [
						{
							AddressHome: "Пушкина 116A",
							CountRoom: 3,
							DescriptionFlat: "67",
							FloorFlat: 4,
							IdFlat: 10,
							TotalArea: 5,
							UserId: 2,
							ImageSrc: 'http://media1.moyareklama.ru/i/p/770x576/201704/20170422175251_b08c6798d750f84f34e25f83fd51b4f5.jpg',
							Reviews: "70%"
						},
						{
							AddressHome: "Есенина 18A",
							CountRoom: 3,
							DescriptionFlat: "67",
							FloorFlat: 4,
							IdFlat: 10,
							TotalArea: 5,
							UserId: 2,
							ImageSrc: 'http://media1.moyareklama.ru/i/p/770x576/201712/20171225154840_8694dca78a9c66a60bfd604291e6484e.jpg',
							Reviews: "70%"
						},
						{
							AddressHome: "Бел. Проспект 1",
							CountRoom: 3,
							DescriptionFlat: "67",
							FloorFlat: 4,
							IdFlat: 10,
							TotalArea: 5,
							UserId: 2,
							ImageSrc: 'http://media1.moyareklama.ru/i/p/770x576/201802/20180218160529_6a2c04bba9011c38a6d5e9fe945cce80.jpg',
							Reviews: "70%"
						},
						{
							AddressHome: "Пушкина 16A",
							CountRoom: 3,
							DescriptionFlat: "67",
							FloorFlat: 4,
							IdFlat: 10,
							TotalArea: 5,
							UserId: 2,
							ImageSrc: 'http://media1.moyareklama.ru/i/p/770x576/201802/20180217193258_56fcd38b69284629c83ecf87da7eca85.jpg',
							Reviews: "70%"
						},
						{
							AddressHome: "Пушкина 116A",
							CountRoom: 3,
							DescriptionFlat: "67",
							FloorFlat: 4,
							IdFlat: 10,
							TotalArea: 5,
							UserId: 2,
							ImageSrc: 'http://media1.moyareklama.ru/i/p/770x576/201711/20171130232307_9ad6b7f86898f3d64724f554d01aef33.jpg',
							Reviews: "90%"
						},
						{
							AddressHome: "Есенина 18A",
							CountRoom: 3,
							DescriptionFlat: "67",
							FloorFlat: 4,
							IdFlat: 10,
							TotalArea: 5,
							UserId: 2,
							ImageSrc: 'http://media1.moyareklama.ru/i/p/770x576/201802/20180218115212_380a017aded1a76b4fb23f7dd1db7bb8.jpg',
							Reviews: "70%"
						},
						{
							AddressHome: "Бел. Проспект 1",
							CountRoom: 3,
							DescriptionFlat: "67",
							FloorFlat: 4,
							IdFlat: 10,
							TotalArea: 5,
							UserId: 2,
							ImageSrc: 'http://media1.moyareklama.ru/i/p/770x576/201802/20180217094843_0fc2de0880f5e7bb2d43627ffdd7a3e1.jpg',
							Reviews: "60%"
						},
						{
							AddressHome: "Пушкина 16A",
							CountRoom: 3,
							DescriptionFlat: "67",
							FloorFlat: 4,
							IdFlat: 10,
							TotalArea: 5,
							UserId: 2,
							ImageSrc: 'http://media1.moyareklama.ru/i/p/770x576/201706/20170607175644_33fcf0a61fedee9bfcdaff6c1b7184e2.jpg',
							Reviews: "90%"
						}
					]
				}
			})).end();
		})

		.use("/public", express.static("./client/public")) //Публичны файлы

		.use("/", function(req, res, next) {
			clientMarkup(req, res)
				.then(markup => {
					res
						.status(200)
						.send(
							page({
								title: "rentFlat",
								keywords: "This is plase for keywords",
								markup
							})
						)
						.end();
				})
				.catch(error => {
					res
						.status(error.status)
						.send(error.response)
						.end();
				});
		});

	return app;
}
