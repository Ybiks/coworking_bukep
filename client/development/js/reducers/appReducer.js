export default function(state = {}, action) {
	switch (action.type) {
		case "SET_APP":
			return action.payload;

		default:
			return state;
	}
}
