export default function(state = {flatsList: []}, action) {
	switch (action.type) {
		case "FLATS_LOADED":
			return {...state, flatsList: action.data}

		default:
			return state;
	}
}
