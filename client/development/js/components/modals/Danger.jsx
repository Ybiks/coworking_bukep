import React, { Component } from "react";
import ReactModal from "react-modal";
import { ButtonModalExit } from "../ButtonModalExit.jsx";

export default class extends Component {
	constructor(props) {
		super(props);
	}
	render() {
		return (
			<ReactModal
				onAfterOpen={() => {}}
				contentLabel={"danger"}
				isOpen={this.props.modalsState.hasOwnProperty("danger")}
				onRequestClose={() => this.props._unmountModal("danger")}
				className="modal"
				style={{
					overlay: {
						backgroundColor:
							Object.keys(this.props.modalsState).slice(-1) ==
							"danger"
								? "rgba(0,0,0,.66)"
								: "rgba(0,0,0,0)"
					}
				}}
			>
				<ButtonModalExit
					onClick={() => this.props._unmountModal("danger")}
					className="icon button_exit-danger"
				/>

				<div className="modal__form">
					{this.props.modalsState["danger"]}
				</div>
			</ReactModal>
		);
	}
}
