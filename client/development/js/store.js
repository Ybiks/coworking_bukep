import { createStore, applyMiddleware } from "redux";
import promisesMiddleware from "./middlewares/promises";
import { composeWithDevTools } from "redux-devtools-extension";
import logger from "redux-logger";
import reducer from "./reducers";

const createStoreWithMiddleware = applyMiddleware(promisesMiddleware, logger)(
	createStore
);

export default function(initialState = {}) {
	return createStoreWithMiddleware(reducer, initialState);
}
