import React from "react";
import { Route, IndexRoute } from "react-router";
import App from "./components/App.jsx";
import * as Pages from "./components/pages";

export default (
	<Route path="/" component={App}>
		<IndexRoute component={Pages.Home} />
		<Route path="/tenant/flat/:idFlat" component={Pages.Flat} />
		<Route path="/tenant" component={Pages.Tenant} />
		<Route path="/lease" component={Pages.Lease} />
		<Route path="*" component={Pages.NotFound} />
	</Route>
);
