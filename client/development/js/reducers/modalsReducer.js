export default function(state = {}, action) {
	console.log("action", action);
	switch (action.type) {
		case "MOUNT_MODAL":
			return (function() {
				if (
					!action.payload.name ||
					typeof action.payload.name !== "string"
				)
					return state;

				return {
					...state,
					[action.payload.name]: action.payload.settings
				};
			})();

		case "UNMOUNT_MODAL":
			return (function() {
				if (!action.payload) return state;

				let copyState = { ...state };

				delete copyState[action.payload];

				return copyState;
			})();

		default:
			return state;
	}
}
