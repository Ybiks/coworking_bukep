import { routerReducer } from "react-router-redux";
import { combineReducers } from "redux";
import app from "./appReducer";
import modals from "./modalsReducer";
import user from "./userReducer";
import customRouterReducer from "./customRouterReducer";
import locale from "./localeReducer";
import flats from './flatsReducer';

export default combineReducers({
	routing: routerReducer,
	customRouterReducer,
	app,
	modals,
	user,
	locale,
	flats
});
