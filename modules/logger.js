let winston = require('winston');

module.exports = (module) => {
	if(module.filename.match(/buildServer.js$/))
		return new winston.Logger({
			transports: []
		})


	return new winston.Logger({ 
		transports: [
			new winston.transports.Console({
				timestamp: true,
				colorize: true,
				level: 'info'
			}),

			new winston.transports.File({ filename: 'debag.log', level: 'debug' })
		]
	})
}