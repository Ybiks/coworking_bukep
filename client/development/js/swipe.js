export default (MenuBar, Mover, Button) => {
	let touch, //Обьект косания
		vector = [],
		swipeTo = "right", //Сторона смещения
		offsetX,
		offsetY, //Первоночальный отступ
		started,
		detecting, //Треггеры
		classAnimate = "animate-swipe",
		maxMarginRight = MenuBar.clientWidth;

	Button.onclick = function() {
		started = true;
		swipeTo = swipeTo == "left" ? "right" : "left";
		swipe();
	};

	Mover.addEventListener("touchstart", touchStart, false);
	Mover.addEventListener("touchmove", touchMove, false);
	Mover.addEventListener("touchend", touchEnd, false);
	Mover.addEventListener("touchcancel", touchEnd, false);

	function touchStart(e) {
		maxMarginRight = MenuBar.clientWidth;
		if (e.touches.length != 1 || started) {
			return;
		}
		Mover.classList.remove(classAnimate);
		detecting = true;
		touch = e.changedTouches[0];
		offsetX = touch.pageX;
		offsetY = touch.pageY;
	}

	function touchMove(e) {
		if (!started && !detecting) {
			return;
		}
		let newTouch = e.changedTouches[0];
		let newX = newTouch.pageX;
		let newY = newTouch.pageY;

		if (detecting) {
			detect();
		}
		if (started) {
			draw();
		}

		function detect() {
			if (newTouch === touch) {
				return;
			}
			if (Math.abs(offsetX - newX) >= Math.abs(offsetY - newY)) {
				e.preventDefault();
				started = true;
			}
			detecting = false;
		}

		function draw() {
			e.preventDefault();

			if (newTouch === touch) {
				return;
			}

			let delta;

			delta =
				swipeTo == "right"
					? offsetX - newX
					: (delta = maxMarginRight + (offsetX - newX));
			delta = delta < 0 ? 0 : delta;
			delta = delta > maxMarginRight ? maxMarginRight : delta;

			if (vector.length > 60) {
				vector.splice(-2);
			}

			Mover.style.transform = "translate(-" + delta + "px, 0)";
			Mover.style.transform = "translate3d(-" + delta + "px, 0, 0)";

			vector.push(delta);
		}
	}

	function touchEnd(e) {
		vector = vector.splice(-2);
		if (started) {
			e.preventDefault();
			if (vector[0] + vector[1] == 0) {
				swipeTo = "right";
			} else if (vector[1] >= vector[0]) {
				swipeTo = "left";
			} else {
				swipeTo = "right";
			}
		}

		swipe(swipeTo);
	}

	function swipe() {
		if (started) {
			Mover.classList.remove(classAnimate);
			Mover.classList.add(classAnimate);
			if (swipeTo == "right") {
				Mover.style.transform = "translate(0, 0)";
				Mover.style.transform = "translate3d(0, 0, 0)";
			} else {
				Mover.style.transform =
					"translate(-" + maxMarginRight + "px, 0)";
				Mover.style.transform =
					"translate3d(-" + maxMarginRight + "px, 0, 0)";
			}
		}
		started = false;
	}

	// удалить класс
	function removeClass(obj, cls) {
		let classes = obj.className.split(" ");

		for (let i = 0; i < classes.length; i++) {
			if (classes[i] == cls) {
				classes.splice(i, 1);
				i--; // (*)
			}
		}
		obj.className = classes.join(" ");
	}
};
