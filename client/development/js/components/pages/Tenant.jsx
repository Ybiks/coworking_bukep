import React from "react";
import { Link } from "react-router";
import { HistoryElement } from "../../libs/HistoryElement.jsx";
import AllParentSize from "../../libs/AllParentSize.jsx";
import ReactTooltip from "react-tooltip";

class Apartament extends React.Component {
	declension(num) {
		var cases = [2, 0, 1, 1, 1, 2];
		return (
			num +
			[" комната", " комнаты", " комнат"][
				num % 100 > 4 && num % 100 < 20
					? 2
					: cases[num % 10 < 5 ? num % 10 : 5]
			]
		);
	}
	goToPage(e) {
		//event-element - класс обозначающий что у элемента есть свой метод, и переход на страницу квартиры не нужен
		if (e.target.className.search("event-element") != -1) {
			e.preventDefault();
			return;
		}

	}
	render() {
		return (
			<Link to={'/tenant/flat/' + this.props.IdFlat} className="tenant" onClick={this.goToPage}>
				<div className="tenant__overlay">
					<div
						className="tenant__star"
						data-tip="Добавить в сохранённые"
					>
						<span className="icon event-element">&#xe82a;</span>
					</div>
				</div>
				<div className="tenant__image">
					<div className="tenant__image-container">
						<img
							className="tenant__img"
							src={this.props.ImageSrc}
						/>
					</div>
				</div>
				<div className="tenant__description">
					<p className="tenant__street" data-tip="Адрес квартиры">
						<span className="icon">&#xe812;</span>
						{this.props.AddressHome}
					</p>
					<p className="tenant__description-bottom">
						<span
							className="tenant__reviews"
							data-tip="Положительность отзывов"
						>
							<span className="icon">&#xe828;</span>
							{this.props.Reviews}
						</span>
						<span data-tip="Количество комнат">
							{this.declension(this.props.CountRoom)}
						</span>
						<span
							className="tenant__price"
							data-tip="Стоимость проживания"
						>
							<span className="icon">&#xe807;</span>
							1200/сут.
						</span>
					</p>
				</div>
			</Link>
		);
	}
}

class Tenant extends React.Component {
	constructor(props) {
		super(props);

		if(this.props.state.flats.flatsList.length < 1)
			this.props.state._loadFlats();
	}
	render() {
		let $this = this;
		return (
			<HistoryElement
				pathname="/tenant"
				style={{
					backgroundColor: "#ebeff1",
					overflowY: "auto",
					height: "100%"
				}}
			>
				<ReactTooltip />
				<div className="filter">
					<AllParentSize className="filter__element" />
				</div>
				<div className="grid grid--container">
					<div className="row">
						{this.props.state.flats.flatsList.map((element, i) => {
							return (
								<div
									key={i}
									className="col col--lg-3 col--md-4 col--sm-6 col--xs-12"
								>
									<Apartament {...element} />
								</div>
							);
						})}
					</div>
				</div>
			</HistoryElement>
		);
	}
}

export default Tenant;
