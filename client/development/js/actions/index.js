import * as api from '../api';
//METODS MODAL
export let _mountModal = (name, settings) => ({
	type: "MOUNT_MODAL",
	payload: { name, settings }
});

export let _unmountModal = contentLabel => ({
	type: "UNMOUNT_MODAL",
	payload: contentLabel
});

export let _setUser = user => ({ type: "SET_USER", payload: user });

//METODS LOCALE
export let _setLocale = prefix => ({ type: "SET_LANGUAGE", payload: prefix });

//METODS FLAT
export let _loadFlats = () => ({
	type: "PROMISE",
	actions: ["FLATS_LOADING", "FLATS_LOADED", "FLATS_LOAD_FAILURE"],
	promise: api.getFlats()
});