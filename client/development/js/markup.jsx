import config from "config";
import React from "react";
import ReactDOM from "react-dom/server";
import { Provider } from "react-redux";
import createStore from "./store";
import fetch from "isomorphic-fetch";
import { match, RouterContext } from "react-router";
import { syncHistoryWithStore } from "react-router-redux";
import routes from "./routes.jsx";
import { polyfill } from "es6-promise";
polyfill();

function getDefaultState() {
	return fetch(`${config.get('linkAPI')}/getDefaultState`, { method: "post" }).then(r =>
		r.json()
	)
}

//Модуль возвращает promise
export default function(req, res) {
	if (process.env.NODE_ENV === "production") {
		return global.cache
			.memoize("defaultState", 60, getDefaultState) //Берем из кэша default state, c квартирами и прочим, последний аргумент, "разогревает" кэш
			.then(state => {
				const store = createStore(state);
				let result;

				//Маршрутизация средствами react-router
				match(
					{ routes, location: req.url },
					(error, redirectLocation, renderProps) => {
						if (redirectLocation)
							// Если необходимо сделать redirect
							return Promise.reject({
								status: 301,
								responce:
									redirectLocation.pathname +
									redirectLocation.search
							});

						if (error)
							// Произошла ошибка любого рода
							return Promise.reject({
								status: 500,
								responce: error.message
							});

						if (!renderProps)
							// Мы не определили путь, который бы подошел для URL
							return Promise.reject({
								status: 404,
								responce: "Page Not Found"
							});

						result = Promise.resolve(
							ReactDOM.renderToString(
								<Provider store={store}>
									<RouterContext {...renderProps} />
								</Provider>
							)
						).then(markup => 
							Promise.resolve(markup += `<script>window.window.INITIAL_STATE = ${JSON.stringify(state)}</script>`)
						);
					}
				);
				return result;
			});
	} else {
		//Для разработки без ошибок о расхождении dom и react dom
		return Promise.resolve('')
	}
}