import Login from "./Login.jsx";
import Danger from "./Danger.jsx";
import Registration from "./Registration.jsx";

export { Login, Danger, Registration };
