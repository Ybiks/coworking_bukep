import React, { Component } from "react";
import ReactModal from "react-modal";
import { ButtonModalExit } from "../ButtonModalExit.jsx";

export default class extends Component {
	constructor(props) {
		super(props);
		this.loginSubmit = this.loginSubmit.bind(this);
	}
	loginSubmit(e) {
		e.preventDefault();
		this.props._mountModal("danger", <h1>Dang</h1>);

		// fetch("/login", {
		// 	method: 'POST',
		// 	headers: {
		// 		'Accept': 'application/json, text/plain, */*',
		// 		'Content-Type': 'application/json'
		// 	},
		// 	credentials: 'same-origin',
		// 	body: JSON.stringify({
		// 		username: ReactDOM.findDOMNode(this.refs.loginUsername).value,
		// 		password: ReactDOM.findDOMNode(this.refs.loginPassword).value
		// 	})
		// }).then((res) => {
		// 		if(res.status == 202)
		// 			res.json().then((user) => {
		// 				this.props.setUser(user);
		// 				this.props.unmountModal('login');
		// 			})
		// 		else
		// 			this.props.mountModal('401');
		// })
	}
	render() {
		return (
			<ReactModal
				onAfterOpen={() => {}}
				contentLabel={"login"}
				isOpen={this.props.modalsState.hasOwnProperty("login")}
				onRequestClose={() => this.props._unmountModal("login")}
				className="modal"
				style={{
					overlay: {
						backgroundColor:
							Object.keys(this.props.modalsState).slice(-1) ==
							"login"
								? "rgba(0,0,0,.66)"
								: "rgba(0,0,0,0)"
					}
				}}
			>
				<ButtonModalExit
					onClick={() => this.props._unmountModal("login")}
				/>

				<div className="modal__form">
					<h2 className="modal__title">
						{this.props.locale("Login to your account")}
					</h2>

					<form
						action="/login"
						method="post"
						onSubmit={this.loginSubmit}
					>
						<input
							type="text"
							className="input_text-primary"
							placeholder={this.props.locale("Username")}
							ref="loginUsername"
							required
							pattern=".{5,}"
							title={this.props.locale(
								"The value of the field cannot be less than 5 characters"
							)}
						/>

						<br />

						<input
							type="password"
							className="input_text-primary"
							placeholder={this.props.locale("Password")}
							ref="loginPassword"
							required
							pattern=".{6,}"
							title={this.props.locale(
								"The value of the field cannot be less than 6 characters"
							)}
						/>

						<br />

						<button className="button-primary-allWidth">
							{this.props.locale("Login")}
						</button>
					</form>
				</div>

				<p
					className="modal__footer"
					onClick={() => this.props._mountModal("forgotPassword")}
				>
					{this.props.locale("Forgot your password?")}
				</p>
			</ReactModal>
		);
	}
}
