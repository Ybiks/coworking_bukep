/*
	Модуль для сборки бандла
*/
import io from "socket.io-client";
import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import routes from "./routes.jsx";
import createStore from "./store";
import { applyRouterMiddleware, browserHistory, Router } from "react-router";
import { syncHistoryWithStore } from "react-router-redux";

window.apiPath = 'http://192.168.101.1:801';

const store = createStore(window.INITIAL_STATE);

const history = syncHistoryWithStore(browserHistory, store);

ReactDOM.hydrate(
	<Provider store={store}>
		<Router history={history}>{routes}</Router>
	</Provider>,
	document.getElementById("app")
);
