import React, { Component } from "react";
import ReactDOM from "react-dom";
import ReactModal from "react-modal";
import { ButtonModalExit } from "../ButtonModalExit.jsx";
import fetch from "isomorphic-fetch";
import ReactTooltip from "react-tooltip";

export default class extends Component {
	constructor(props) {
		super(props);
		this.checkConfirmPassword = this.checkConfirmPassword.bind(this);
	}

	isConfirmPassword() {
		return (
			this.refs.registrationPassword.value ===
			this.refs.registrationConfirmPassword.value
		);
	}

	checkConfirmPassword() {
		if (!this.isConfirmPassword()) {
			ReactDOM.findDOMNode(
				this.refs.registrationConfirmPassword
			).className +=
				" " + "input_no_walid";

			ReactTooltip.show(
				ReactDOM.findDOMNode(this.refs.registrationConfirmPassword)
			);
		} else {
			ReactDOM.findDOMNode(
				this.refs.registrationConfirmPassword
			).classList.remove("input_no_walid");

			ReactTooltip.hide(
				ReactDOM.findDOMNode(this.refs.registrationConfirmPassword)
			);
		}
	}

	render() {
		return (
			<ReactModal
				onAfterOpen={() => {}}
				contentLabel={"registration"}
				isOpen={this.props.modalsState.hasOwnProperty("registration")}
				onRequestClose={() => this.props._unmountModal("registration")}
				className="modal"
				style={{
					overlay: {
						backgroundColor:
							Object.keys(this.props.modalsState).slice(-1) ==
							"registration"
								? "rgba(0,0,0,.66)"
								: "rgba(0,0,0,0)"
					}
				}}
			>
				<ButtonModalExit
					onClick={() => this.props._unmountModal("registration")}
				/>

				<div className="modal__form">
					<h2 className="modal__title">
						{this.props.locale("Create an account")}
					</h2>

					<form
						action="/registration"
						method="post"
						onSubmit={this.loginSubmit}
					>
						<input
							type="text"
							className="input_text-primary"
							placeholder={this.props.locale("Username")}
							ref="registrationUsername"
							required
							pattern=".{5,}"
							title={this.props.locale(
								"The value of the field cannot be less than 5 characters"
							)}
						/>

						<br />

						<input
							type="password"
							className="input_text-primary"
							placeholder={this.props.locale("Password")}
							ref="registrationPassword"
							required
							pattern=".{6,}"
							title={this.props.locale(
								"The value of the field cannot be less than 6 characters"
							)}
							onChange={this.checkConfirmPassword}
						/>

						<br />

						<input
							type="password"
							className="input_text-primary"
							placeholder={this.props.locale("Confirm password")}
							onChange={this.checkConfirmPassword}
							ref="registrationConfirmPassword"
							required
							data-tip={this.props.locale(
								"Passwords Don't Match"
							)}
						/>

						<ReactTooltip
							place="bottom"
							type="error"
							effect="float"
							data-delay-hide="1000"
							getContent={() => {
								if (this.isConfirmPassword()) return null;
								return this.props.locale(
									"Passwords Don't Match"
								);
							}}
						/>

						<br />

						<input
							type="email"
							className="input_text-primary"
							placeholder="E-mail"
							required
							title={this.props.locale("Your Email")}
						/>

						<br />

						<button className="button-primary-allWidth">
							{this.props.locale("Register")}
						</button>
					</form>
				</div>
			</ReactModal>
		);
	}
}
