export default function(state = {}, action) {
	switch (action.type) {
		case "getApartament":
			return (function() {

				return {
					...state
				};
			})();

		case "UNMOUNT_MODAL":
			return (function() {
				if (!action.payload) return state;

				let copyState = { ...state };

				delete copyState[action.payload];

				return copyState;
			})();

		default:
			return state;
	}
}
