require('babel-core/register');
['.css', '.less', '.sass', '.ttf', '.woff', '.woff2'].forEach((ext) => require.extensions[ext] = () => {});
require('babel-polyfill');
process.env.NODE_ENV === 'production' ? require('./server/buildServer') : require('./server/devServer');