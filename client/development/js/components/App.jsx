import React, { Component } from "react";
import ReactDOM from "react-dom";
import { connect } from "react-redux";
import * as actions from "../actions/";
import { bindActionCreators } from "redux";
import { Link, IndexLink } from "react-router";
import swipe from "../swipe";
import { Login, Danger, Registration } from "./modals";

class App extends Component {
	constructor(props) {
		super(props);
	}

	componentDidMount() {
		swipe(
			ReactDOM.findDOMNode(this.refs.menu),
			ReactDOM.findDOMNode(this.refs.contains),
			ReactDOM.findDOMNode(this.refs.triggerButton)
		);
	}

	renderAccount(user) {
		if (this.props.user !== false) {
			return (
				<Link
					to="/personal-account"
					activeClassName="active_link"
					className="menu_bar_account"
				>
					<img
						className="avatar_min"
						src="/public/images/thumb-83907.jpg"
					/>
					{this.props.user.name}
				</Link>
			);
		} else {
			return (
				<div>
					<button
						className="app__menu-header-button"
						onClick={() => {
							this.props._mountModal("login");
						}}
					>
						{this.props.locale("Login")}
					</button>
					<span>/</span>
					<button
						className="app__menu-header-button"
						onClick={() => {
							this.props._mountModal("registration");
						}}
					>
						{this.props.locale("Register")}
					</button>
				</div>
			);
		}
	}

	render() {
		var $this = this;
		function isLanguage(language) {
			return $this.props.locale("language") == language
				? " app__menu-language-button-active"
				: "";
		}

		return (
			<div>
				<Login
					locale={this.props.locale}
					modalsState={this.props.modals}
					_mountModal={this.props._mountModal}
					_unmountModal={this.props._unmountModal}
				/>
				<Danger
					locale={this.props.locale}
					modalsState={this.props.modals}
					_mountModal={this.props._mountModal}
					_unmountModal={this.props._unmountModal}
				/>
				<Registration
					locale={this.props.locale}
					modalsState={this.props.modals}
					_mountModal={this.props._mountModal}
					_unmountModal={this.props._unmountModal}
				/>

				<section ref="menu" className="app__menu">
					<div className="app__menu-header">
						{this.renderAccount(this.props.user)}
					</div>

					<nav>
						<IndexLink
							to="/"
							className="app__menu-link"
							activeClassName="app__menu-link_active"
						>
							{this.props.locale("Home")}
						</IndexLink>
						<Link
							to="/tenant"
							className="app__menu-link"
							activeClassName="app__menu-link_active"
						>
							{this.props.locale("Tenant")}
						</Link>
						<Link
							to="/lease"
							className="app__menu-link"
							activeClassName="app__menu-link_active"
						>
							{this.props.locale("Lease")}
						</Link>
					</nav>

					<div className="app__menu-language">
						<button
							className={
								"app__menu-language-button" + isLanguage("en")
							}
							onClick={() => this.props._setLocale("en")}
						>
							EN
						</button>

						<button
							className={
								"app__menu-language-button" + isLanguage("ru")
							}
							onClick={() => this.props._setLocale("ru")}
						>
							RU
						</button>
					</div>
				</section>

				<section
					className="app__container animate-swipe"
					ref="contains"
				>
					<button
						className="app__menu-button icon"
						ref="triggerButton"
					>
						&#xe802;
					</button>

					<div className="app__container-page-component">
						{React.cloneElement(this.props.children, {
							state: this.props
						})}
					</div>
				</section>
			</div>
		);
	}
}

export default connect(
	state => ({
		user: state.user,
		app: state.app,
		modals: state.modals,
		locale: state.locale,
		flats: state.flats
	}),
	dispatch => bindActionCreators(actions, dispatch)
)(App);
